<?php

	use Roots\Sage\Extras;

	// ini_set('display_errors', 1);
	// ini_set('display_startup_errors', 1);
	// error_reporting(E_ALL);

	header('Content-Type: application/json');

	if(isset($_GET['date']) ) {
		$date = $_GET['date'];
	} else {
		$date = date('Y-m-d');
	}

	$this_month_start 	= date('Y-m-01', strtotime($date));
	$this_month_end 		= date('Y-m-t', strtotime($date));

	$prev_month_start 	= date('Y-m-01', strtotime("-2 months", strtotime($date)));
	$next_month_end 		= date('Y-m-t', strtotime("+2 months", strtotime($date)));

	$next_three_months 	= date('Y-m-t', strtotime("+3 months", strtotime($date)));
  
  // $date_start = date('Y-m-01', strtotime($dateStart));
  // $date_end 	= date('Y-m-t', strtotime($dateEnd));
  $post_type = $_GET['post_type'];

	// Our include
	define('WP_USE_THEMES', false);
	require_once('../../../wp-load.php');


	$result = array(
		'one_month' 		=> Extras\getEvents($post_type, $this_month_start, $this_month_end),
		'three_months' 	=> Extras\getEvents($post_type, $prev_month_start, $next_month_end),
		'from_today' 		=> Extras\getEvents($post_type, date('Y-m-d'), $next_three_months, 7)
		);

	echo json_encode($result); 
?>