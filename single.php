<?php 
	if (get_post_type() == 'post') {
		// if post - get format
		get_template_part('templates/content-single-post', get_post_format());

	} else {
		// if custom post type - get type
		get_template_part('templates/content-single', get_post_type());
	}	 
?>
