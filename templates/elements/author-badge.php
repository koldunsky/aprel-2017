<?php if(get_field('post_author_id')): 
  $author_id = get_field('post_author_id')[0];
  $author_obj = get_post($author_id); 
?>
	<div class="author-badge">
	  <?php $author_img = types_render_field('expert_avatar', array('id'=>$author_id, 'url'=> true)); ?>
	  <div class="author-badge__img" style='background-image: url(<?php echo $author_img; ?>)'>
	  	
	  </div>
	  <div class="author-badge__name">
	  	<a href="<?php echo $author_obj->guid ?>">
	  		<h5>  			
	  			<?php echo $author_obj->post_title ?>
	  		</h5>
	  	</a>
	  </div>
	</div>
<?php endif; ?>