<script id="clndr-template" type="text/template">
  <div class='clndr__month header__cal__bar'>
    <div class="header__cal__toggler">
      <span class="dashicons dashicons-arrow-down-alt2"></span>
    </div>
    <%= month %>  
    <div class="header__cal__toggler">
      <span class="dashicons dashicons-calendar-alt"></span>      
    </div>
  </div>

  <div class='clndr__controls'>
    <div class='clndr-previous-button'>&lsaquo;</div><div class='clndr-next-button'>&rsaquo;</div>
  </div>   
    <div class="clndr__table">   
      <div class='clndr__table__head'>
        <% for(var i = 0; i < daysOfTheWeek.length; i++) { %>
              <div class='clndr__day clndr__table__head__day'>
                <div class="clndr__day__num">
                  <%= daysOfTheWeek[i] %>
                </div> 
              </div>
          <% } %>
      </div>
      <div class="clndr__table__body">
        <% _.each(days, function(day) { %>
            <div class="<%= day.classes %> clndr__day">
            <% if(typeof day.events !== 'undefined' && !_.isEmpty(day.events)) { %>
              <div class="clndr__event__conainer">                
                <% _.each(day.events, function(event) { %>
                  <div class="clndr__event">


                    <div class="clndr__event__time">
                    <% if(typeof event.startTime !== 'undefined' && !_.isEmpty(event.startTime)) { %>
                      <%= event.startTime.substring(0, 5) %>
                      <% if(typeof event.endTime !== 'undefined' && !_.isEmpty(event.endTime)) { %>
                        -<%= event.endTime.substring(0, 5) %>
                      <% } %>
                    <% } else { %>
                        —
                    <% } %>
                    </div>

                    <div class="clndr__event__title"> 
                      <a href="<%= event.url %>">
                        <%= event.title %>
                      </a>
                    </div>


                  </div> <!-- /clndr__event -->
                <% }); %>
              </div>
            <% } %>
              <div class="clndr__day__num">
                <%= day.day %>
              </div>              
            </div>
        <% }); %>
      </div>
    </div> <!-- .clndr__table -->
</script>