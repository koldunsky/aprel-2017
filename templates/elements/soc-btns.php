<?php 
use Roots\Sage\Extras;
?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.8&appId=535566529966066";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="soc-links__item google-play-badge">
	<h5>Мы на андроид!</h5>
	<div class="soc-links__item__description">
		Программа рассчета нумерологического теста «Идеал» под Андроид
	</div>
	<?php 
		$google_play_link = get_theme_mod('google_play_link');
		if($google_play_link) {

			echo '<a href="'.$google_play_link.'">';
				echo '<img src="/wp-content/themes/aprel/assets/images/google-play.png">';
			echo '</a>';
		}
	?>
</div>
<div class="soc-links__item  facebook-badge">
	<h5>Станьте нашим другом на facebook</h5>


	<a href="https://www.facebook.com/aplaprel/" target="_blank">
		<button class="facebook-badge__btn">
			<span class="dashicons dashicons-facebook"></span> На страницу &rarr;
		</button>
	</a>
	<div class="fb-like" data-width="150" data-href="https://www.facebook.com/aplaprel/" data-layout="button_count" data-action="like" data-size="large" data-show-faces="false" data-share="false"></div>
</div>