<?php 
use Roots\Sage\Extras;
?>

<?php while (have_posts()) : the_post(); ?>
  <?php $postLink = esc_url( Extras\get_link_url() ); ?>
  <article <?php post_class(); ?>>
    <header class="page-header">
      <h1 class="entry-title"><?php the_title(); ?></h1>
      <?php get_template_part('templates/entry-meta'); ?>
    </header>
    <div class="entry-content">      
          <h2>Перейдите по ссылке: <a href="<?php echo $postLink; ?>" target="_blank"><?php echo $postLink; ?></a></h2>        
      </h2>
    </div>
  </article>
<?php endwhile; ?>
