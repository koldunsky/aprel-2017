<article <?php post_class('col-xs-12 col-sm-6 col-md-6 col-xl-3'); ?>>
  <header>
    <h3 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

    <?php get_template_part('templates/entry-meta'); ?>
  </header>
  <div class="entry-summary">
    <?php the_excerpt(); ?>
  </div>
  <footer>  
    <?php get_template_part('templates/elements/author-badge'); ?>
  </footer>
</article>
