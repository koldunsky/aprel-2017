<?php if (get_the_category()[0]->slug == 'news'): ?>
	<time class="updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time>
<?php endif ?>