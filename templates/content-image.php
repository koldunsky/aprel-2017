<?php 
use Roots\Sage\Extras;
 ?>

<?php 
  if( has_post_thumbnail() ) {
    $img = get_the_post_thumbnail_url();
  } else {
    $img = '/wp-content/themes/aprel/static/img/numbers-wallpaper-1920x1200-1024x640.jpg';
  } 
?>
<article <?php post_class('col-xs-12 col-sm-6 col-md-12 col-lg-6 col-xl-4 col-xxxl-3'); ?>>
<div class="video-post__inner">  
  <div class="video-post__video"> 
      <a href="<?php the_permalink(); ?>">     
    <div class="video-post__video__container">   
        <div style="background-image: url(<?php echo $img; ?>);"></div>     
    </div>
      </a>    
  </div>
  <div class="video-post__content">
    <header>
      <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
      <?php get_template_part('templates/entry-meta'); ?>
    </header>
  </div>
</div>
</article>