<script src="https://unpkg.com/masonry-layout@4.1/dist/masonry.pkgd.min.js"></script>
<?php get_template_part('templates/elements/clndr'); ?>
<?php get_template_part('templates/elements/event-grid-item'); ?>
<footer class="content-info site-footer">
	<?php dynamic_sidebar('sidebar-footer'); ?>

	<div class="copyright">
	</div>
		<script>
			var top_bubble_imgs = "<?php echo esc_url( get_theme_mod( 'custom_bubbles' ) ); ?>";
		</script>
</footer>