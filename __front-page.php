<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>
<div class="aprel-post-slider owl-carousel">
<?php 
//$frontpage_posts = new WP_Query( array( 'posts_per_page' => 3, 'category_name' => 'news' ) );
$frontpage_posts = new WP_Query( array( 'posts_per_page' => 5, 'post_type' => ['post', 'events'] ) );

if( $frontpage_posts->have_posts() ) :
	/*here possibly a FrontPage Posts title or so ? */
	while( $frontpage_posts->have_posts() ) :
		$frontpage_posts->the_post();
		get_template_part('templates/slider', get_post_type() != 'post' ? get_post_type() : get_post_format());
	endwhile;
endif; wp_reset_postdata();
?>
</div>
<div class="entry-content">
	<?php the_content(); ?>
</div>
<div class="mp-promo">
<?php 
	// check if the repeater field has rows of data
	if( have_rows('mp_promo_block') ):
		$block_counter = 1;
		$blocks_arr = array();
	 	// loop through the rows of data
	  while ( have_rows('mp_promo_block') ) : the_row();
	  	$bubbles_counter = 1;
			$bubbles_arr = array();
			while ( have_rows('mp_bubbles') ) : the_row();
				$bubble_pos_raw = array_map('trim', explode(',', get_sub_field('mp_bubble_pos')));
				$bubble_pos = array(
					'width' => $bubble_pos_raw[0],
					'top' => $bubble_pos_raw[1],
					'right' => $bubble_pos_raw[2],
					'bottom' => $bubble_pos_raw[3],
					'left' => $bubble_pos_raw[4]
					);
				$bubbles_arr[] = array(
					'img' 	=> get_sub_field('mp_bubble_img'),
					'title' => get_sub_field('mp_bubble_title'),
					'desc' 	=> get_sub_field('mp_bubble_desc'),
					'link' 	=> get_sub_field('mp_bubble_link'),
					'pos' 	=> $bubble_pos,
					'id' 		=> $bubbles_counter
			);
			$bubbles_counter++;
			endwhile;

			$blocks_arr[] = array(
				'desc' 		=> get_sub_field('mp_block_desc'),
				'color' 	=> get_sub_field('mp_block_color'),
				'id' 			=> $block_counter,
				'bubbles' => $bubbles_arr
			);
		$block_counter++;
	  endwhile;
	endif;
?>	
<?php foreach ($blocks_arr as $block) : ?>
	<div class="mp-promo__block mp-promo__block-<?php echo $block['id']; ?>">
		<h2 class="mp-promo__block__numerator">
			<strong>
				<?php echo $block['id'] . "."; ?>
			</strong>
		</h2>
		<div class="mp-promo__block__content clear">
			<div class="col-lg-4 col-xl-6">		
				<?php echo $block['desc']; ?>
			</div>		
			<div class="
				col-lg-offset-4 
				col-lg-8 

				col-xl-offset-6 
				col-xl-6
				mp-promo__bubbles__container
			">
				<div class="mp-promo__bubbles__bg-layer">
					<?php $bubble_counter = 1; ?>
					<?php $bubbles_arr_size = count($block['bubbles']); ?>
					<?php foreach ($block['bubbles'] as $bubble) : ?>
						<?php
							$styles = array();
							$styles['background-image'] = 'url('.$bubble['img'].')';
							$styles['border-color'] = $block['color'];
							foreach ($bubble['pos'] as $key => $val) {
								if(isset($val))
									$styles[$key] = $val;
							}
						?>
					<div 	class="
									mp-bubble__bg									
									js-bubble-parallax-layer-<?php echo $bubble_counter; ?>
									<?php
										if( !$bubble['img'] )
											echo 'has-no-img-bg';
									?>
								" 
								data-bubble-id="<?php 
									echo $block['id'];  
									echo $bubble['id'];  
								?>"
								data-styles='<?php echo json_encode($styles); ?>'
								>	
								<h3 class="mp-bubble__title">
									<?php echo $bubble['title']; ?>
								</h3>
								<div class="mp-bubble__description">
									<?php echo $bubble['desc']; ?>
									<?php if(strlen($bubble['link'])): ?>
										<a 
											class="mp-bubble__link" 
											title="<?php echo $bubble['title'];?>" 
											href="/<?php echo $bubble['link'];?>"
										>
												Узнать подробнее &rarr;
										</a>
								<?php endif; ?>
								</div>	
								<div class="mp-bubble__close">
									<span class="dashicons dashicons-no-alt"></span>
								</div>
					</div>
						<?php $bubble_counter++; ?>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	<style>
		.mp-promo__block-<?php echo $block['id']; ?> strong,
		.mp-promo__block-<?php echo $block['id']; ?> b {
			color: <?php echo $block['color']; ?> !important;
		}
		.mp-promo__block-<?php echo $block['id']; ?> .mp-bubble__bg::before {
			content: '';
			position: absolute;
			background: <?php echo $block['color']; ?> !important;
			width: 100%;
			height: 100%;
			top: 0;
			left: 0;
			opacity: .35;
			border-radius: 50%;
		}
	</style>
</div>
<?php endforeach; ?>
</div>