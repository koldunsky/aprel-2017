var eventsForClndr = {
	header: []
};

function getDataClndr(clndr, eventsHolder) {
	var month = clndr.month;
	return $.get('/wp-content/themes/aprel/rest_handler.php',
		{
			date: month.format('YYYY-MM-DD'),
			post_type: 'events'
		}
	)	
}

function fillTopEventsGrid(events) {
	var headerGrid 		= $('#header__cal__grid');
	headerGrid.html(''); // reset

	var template 			= _.template($('#event-grid-item').html()),
			sortedEvents 	= _.sortBy(events, function (event) {return event.date ? event.date : event.startDate});

	_.each(sortedEvents, function (event, i , events) {
		$('#header__cal__grid').append(template(event));
	});	
}
if($('#header__clndr:visible').length) {	
	$('#header__clndr').clndr({
		daysOfTheWeek: ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'ВС'],
		template: $('#clndr-template').html(),
	  multiDayEvents: {
	    endDate: 'endDate',
	    singleDay: 'date',
	    startDate: 'startDate'
	  }, 
	  ready: function () { 
	  	var clndr = this;
	  	getDataClndr(this, eventsForClndr.header)
	  	.done(function (events) {
				fillTopEventsGrid(events.from_today);
	  		clndr.setEvents(events.three_months);
				initHeaderClndrScroll();
	  	});
	  },
	  clickEvents: {
	    onMonthChange: function (month) {    	
		  	var clndr = this;
		  	getDataClndr(this, eventsForClndr.header)
		  	.done(function (events) {	  		
	  			clndr.setEvents(events.three_months);
		  	});
	    }
	  }
	});
}

$(document).on('click', '.header__cal .clndr__month', function () {
	$('.header__cal').toggleClass('header__cal_expanded');
});
function initHeaderClndrScroll() {
	$("#header__cal__grid").mCustomScrollbar({
		axis:"x",
		theme:"dark-thin",
		mouseWheel:{ scrollAmount: 40 },
		scrollInertia: 0,
		autoExpandScrollbar:true,
		advanced:{autoExpandHorizontalScroll:true}
	});
}

function clndrTooltip(e) {	
	$('.event', $('.clndr')).removeClass('event_active');

	if($(e.target).closest('.event').length) {
		$(e.target).closest('.event').addClass('event_active');
	}
}

$(document).on('click', clndrTooltip);
