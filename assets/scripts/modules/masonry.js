
$(window).load(function() {
  startMasonry();
});

function startMasonry() {
	var $masonryLoop = $('.js-masonry-loop');
	$masonryLoop.masonry({
    // options...
    itemSelector: '.hentry',
    columnWidth: '.hentry',
    percentPosition: true
  });
}