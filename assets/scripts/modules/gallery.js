
;
$('.gallery').each(function (i, el) {
    var wpGalleryOwl = $(el);
    wpGalleryOwl.owlCarousel({
        loop: true,
        responsiveClass: true,
        lazyLoad: true, 
        margin: 10,
        autoWidth: true,
        center: true,
        nav: false,
        responsive:{
            0:{
                items:1,
                loop:true
            }, 
            768:{
                items:3,
                loop:true
            },
            1200: {           
                items:5,
                loop:true
            }
        }
    });

    wpGalleryOwl.append(
        $('<div>').attr('class','owl-next')
        .on('click', function () {
            wpGalleryOwl.trigger('next.owl.carousel');
        })
    );

    wpGalleryOwl.append(
        $('<div>').attr('class','owl-prev')
        .on('click', function () {
            wpGalleryOwl.trigger('prev.owl.carousel');
        })
    );
});