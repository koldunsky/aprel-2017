$('.js-nav-primary-toggler').click(function (e) {
	e.preventDefault();
	openMobileMenu('.nav-primary',e.currentTarget);
	console.log(e);
});

$('.js-nav-right-sidebar-toggler').click(function (e) {
	e.preventDefault();
	openMobileMenu('.nav-right-sidebar',e.currentTarget);
	console.log(e);
});

function openMobileMenu(menuClass, clickedEl) {
	var currenMenu = false;
	var currenEL = false;

	$(menuClass).hasClass('menu-opened') ? currenMenu = true : currenMenu = false;
	$(clickedEl).hasClass('menu-item-active') ? currenEL = true : currenEL = false;

	$('.menu-opened').removeClass('menu-opened');
	$('.menu-item-active').removeClass('menu-item-active');

	if(!currenEL) {		
		$(clickedEl).addClass('menu-item-active');
	}
	if(!currenMenu) {
		$(menuClass).addClass('menu-opened');
	}
}