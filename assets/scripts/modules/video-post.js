  /*  lazy loading for youtube videos  */
    function buildYtPlaceholders() {
      var v = $(".youtube-player:not(.iframe-video-added)");
      for (var n = 0; n < v.length; n++) {
        var p = document.createElement("div");
        p.innerHTML = '<div class="play-button"><span class="dashicons dashicons-video-alt3"></span></div>';
        var bgImg = v.data('background') || YtThumb(v[n].dataset.videoid);
        $(p).css('background-image', 'url(' + bgImg + ')');
        p.onclick = YtIframe;
        v[n].appendChild(p);
        $(v[n]).addClass('iframe-video-added');
      }
    }
     
    function YtThumb(id) {
      return '//i.ytimg.com/vi/' + id + '/hqdefault.jpg';
    }
     
    function YtIframe() {
      var iframe = document.createElement("iframe");
      iframe.setAttribute("src", "//www.youtube.com/embed/" + this.parentNode.dataset.videoid + "?autoplay=1&fs=2&autohide=2&border=0&wmode=opaque&enablejsapi=1&controls=1&showinfo=0");
      iframe.setAttribute("frameborder", "0");
      iframe.setAttribute("class", "youtube-iframe");
      iframe.setAttribute("allowfullscreen","");
      iframe.setAttribute("mozallowfullscreen","");
      iframe.setAttribute("webkitallowfullscreen","");
      this.parentNode.replaceChild(iframe, this);
    }

    /*  lazy loading for youtube videos END  */ 
      
    /*  lazy loading for vimeo videos  */
    function buildVimeoPlaceholders() {

      var v = $(".vimeo-player:not(.iframe-video-added)");
      for (var n = 0; n < v.length; n++) {
        (function(){
          var videoContainer = v[n],
          videoId = videoContainer.dataset.videoid,
          promise = $.getJSON('http://www.vimeo.com/api/v2/video/' + videoId + '.json?callback=?');
          
          promise.done(function(data) {   
            var p = document.createElement("div");
            p.innerHTML = '<div class="play-button"><span class="dashicons dashicons-video-alt3"></span></div>';
            var bgImg = v.data('background') || data[0].thumbnail_large;
            $(p).css('background-image', 'url(' + bgImg + ')');
            p.onclick = vimeolIframe;                
            $(videoContainer).addClass('iframe-video-added');
            videoContainer.appendChild(p);
          }); 
        })();           
      }
    }
       
    function vimeolIframe() {
      var iframe = document.createElement("iframe");
      iframe.setAttribute("src", "//player.vimeo.com/video/" + this.parentNode.dataset.videoid + "?autoplay=1&title=0&byline=0&portrait=0&color=E85433");
      iframe.setAttribute("frameborder", "0");
      iframe.setAttribute("class", "vimeo-iframe");
      iframe.setAttribute("allowfullscreen","");
      iframe.setAttribute("mozallowfullscreen","");
      iframe.setAttribute("webkitallowfullscreen","");
      this.parentNode.replaceChild(iframe, this);
    }
    /*  lazy loading for vimeo videos END  */ 

    function videoPostLayout(container, video, content) {
        $(container).each(function (i, el) {
          var $el = $(el),
          $video = $(video, $el);
          $content = $(content, $el),
          $contentPadding = $content.outerHeight() - $content.height(),
          $contentHeader = $('header', $content),
          $contentExcerpt = $('.entry-summary', $content);

          if(aprel.device === 'desktop' ) {  
            videoContentHeight = $video.height() - $contentPadding - $contentHeader.outerHeight(true);
          } else {
            videoContentHeight = 'auto';
          }
          
          $contentExcerpt.outerHeight(videoContentHeight);
        });
    }


        
    buildYtPlaceholders();

    buildVimeoPlaceholders();

    $(window).on('aprel:resize', function () {
      videoPostLayout('.post.format-video', '.video-post__video', '.video-post__content');
      
      (function () {
        setTimeout(startMasonry, 500);
      })();
    });

    videoPostLayout('.post.format-video', '.video-post__video', '.video-post__content');