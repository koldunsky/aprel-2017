$(document).on('click', '.team-member__name, .team-member__img',function (e) {
	var $el = $(e.target),
	$parent = $el.parent('.team-member');
	$block = $el.parents('.our-team-block');

	$('.team-member_active').removeClass('team-member_active');
	$parent.addClass('team-member_active');	
	$block.addClass('our-team-block_active');	

	if(aprel.device != 'mobile' && aprel.device != 'tablet' ) {		
		if($('.main').offset().top > $(window).scrollTop()) {
			goToByScroll('.main');
		}
	}
});

$(document).on('click', '.team-member_active .team-member__name, .team-member_active .team-member__img, .team-member .js-team-member__close-excerpt',function (e) {
	$('.team-member_active').removeClass('team-member_active');
	$('.our-team-block_active').removeClass('our-team-block_active');
});

$('.team-member__soc-links').each(function () {
	var rawIcons = $(this).text().trim();
	var newHtml = '';
	if (rawIcons.length) {		
		rawIcons.split(',').forEach(function (val, i, arr) {
			socLinkData = val.split('::');
			newHtml += '<a href="' + socLinkData[1] + '" class="team-member__soc-links__item socicon-' + socLinkData[0].trim() + '" target="_blank"></a>';
		});
	}
	$(this).html(newHtml).show();
});

$('.js-expert-opinion-load_posts').click(function () {
	var container = $('.js-expert-opinion-loop');
	var post_loaded = container.data('loaded');
	var offset = container.data('offset');
	var expert_id = container.data('expert_id');

	get_experts_posts(offset);
});

function get_experts_posts(offset) {
	$.get( "/wp-json/wp/v2/posts/?filter[category_name]=expert_opinion", function( data ) {
		data.splice(0, offset);
		if($("#expert-opinion-post").length && data.length) {
			var expert_opinion_posts_tpl_raw = $("#expert-opinion-post").html();
			var expert_opinion_posts_tpl = Handlebars.compile(expert_opinion_posts_tpl_raw);
			var expert_opinion_posts_content = expert_opinion_posts_tpl(data);
			$('.js-expert-opinion-loop').append(expert_opinion_posts_content);

			console.log('Handelbars loop in "our-team.js"');
			console.log(data);
		}
	});
}