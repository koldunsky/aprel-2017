
function fillEventsGrid(events) {
	var headerGrid 		= $('#clndr__full__grid');
	headerGrid.html(''); // reset

	var template 			= _.template($('#event-grid-item').html()),
			sortedEvents 	= _.sortBy(events, function (event) {return event.date ? event.date : event.startDate});

	if(!_.isEmpty(events)) {
		_.each(sortedEvents, function (event, i , events) {
			$('#clndr__full__grid').append(template(event));
		});	
	} else {
		$('#clndr__full__grid').append('<h2 class="h1" style="font-weight: 200;">В этом месяце нет событий</h2>');
	}
}
if ($('#clndr-full').length) {	
	$('#clndr-full').clndr({
		daysOfTheWeek: ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'ВС'],
		template: $('#clndr-full-template').html(),
	  multiDayEvents: {
	    endDate: 'endDate',
	    singleDay: 'date',
	    startDate: 'startDate'
	  }, 
	  ready: function () { 
	  	var clndr = this;
	  	getDataClndr(this, eventsForClndr.header)
	  	.done(function (events) {
	  		clndr.setEvents(events.three_months);
	  		fillEventsGrid(events.one_month);
	  	});
	  },
	  clickEvents: {
	    onMonthChange: function (month) {    	
		  	var clndr = this;
		  	getDataClndr(this, eventsForClndr.header)
		  	.done(function (events) {	  		
	  			clndr.setEvents(events.three_months);
	  			fillEventsGrid(events.one_month);
		  	});
	    },
	    click: function (target) {
	    	console.log(target);
	    }
	  }
	});
}

function toggleFullCalGrid() {
	$('.clndr__controls__grid').toggleClass('active');
	$('.clndr__controls__calendar').toggleClass('active');
	$('body').toggleClass('events_grid_active');

	if($('body').hasClass('events_grid_active')) {
		window.location.hash = 'grid';
	} else {
		window.location.hash = 'calendar';
	}		
}



$(document).on('click', '.clndr__controls__grid:not(.active), .clndr__controls__calendar:not(.active)', function (e) {
	toggleFullCalGrid();
});

// localstorage hash
function lsTest(){
    var test = 'aprel';
    try {
        localStorage.setItem(test, test);
        localStorage.removeItem(test);
        return true;
    } catch(e) {
        return false;
    }
}

function eventGridCtrl() {
	window.location.hash === '#calendar' ? toggleFullCalGrid() : null;
}

$(window).load(eventGridCtrl);