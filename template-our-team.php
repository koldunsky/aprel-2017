<?php
/**
 * Template Name: Наша команда
 */
?>
<section>	
	<?php while (have_posts()) : the_post(); ?>
	  <article <?php post_class(); ?>>
	    <header class="page-header">
	      <h1 class="entry-title"><?php the_title(); ?></h1>
	    </header>
	    <div class="entry-content">
	      <?php the_content(); ?>
	    </div>
	  </article>
	<?php endwhile; ?>
</section>
<section>
	<?php
		$step = 6;
	  // set up or arguments for our custom query
	  $paged = ( get_query_var('page') ) ? get_query_var('page') : 1;
	  $query_args = array(
	    'post_type' => 'post',
	    'category_name' => 'expert_opinion',
	    'posts_per_page' => $step,
	    'paged' => $paged
	  );
	  // create a new instance of WP_Query
	  $the_query = new WP_Query( $query_args );
	?>

	<?php if ( $the_query->have_posts() ): ?> 
	<div class="page-header row">
		<h2 class="h1">Экспертное мнение</h2>
	</div>
	<div class="expert-opinion-loop js-expert-opinion-loop row js-masonry-loop archive" data-loaded='<?= $step ?>' data-step='<?= $step ?>'>
		<?php while ( $the_query->have_posts() ) : $the_query->the_post(); // run the loop ?>		
			  <?php get_template_part('templates/content', 'expert-opinion'); ?>
		<?php endwhile; ?>
	</div>
	<div style="text-align: center;">		
		<a href="/category/expert_opinion"><button class="btn btn-primary">Посмотреть все записи</button></a>
	</div>
	<?php endif; ?>

	<script id="expert-opinion-post"  type="text/x-handlebars-template">
	{{#each this}}
		<article class="col-md-6 col-xl-3">
	    <h3 style="color:red">{{title.rendered}}</h3>
	    <div class="excerpt">
	    </div>
	  </article>
	{{/each}}
	</script>
</section>